#!/bin/bash

oldLog=old-perf.log
newLog=perf.log

mv -f $newLog $oldLog 2> /dev/null

# start the server
cpserver &
sleep 0.5

# load our handler
cpserver --load PrPixelCudaHandler
sleep 0.5

echo sending data
cpdriver --data ~/pr-data-100

# clean up
cpserver --exit

if [ -f $oldLog ]; then
	./compare-perf.py old-perf.log perf.log
else
	echo $newLog generated
fi
