#!/bin/bash

# start the server
cpserver &
sleep 0.5

# load our handler
cpserver --load PrPixelCudaHandler
sleep 0.5

# test against recorded sample data
file=correctness-report.txt
rm $file
cpdriver --data ~/pr-data-100 --verify &> $file

# clean up
cpserver --exit

# show the results
less correctness-report.txt
